package main;

import java.io.*;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TCPsocket
{
    public static final int BUFFER_SIZE = 128;
    private DataInputStream in;
    private DataOutputStream out;

    /**
     * sets the streams
     * @param in
     * @param out
     */
    public TCPsocket(InputStream in, OutputStream out)
    {
        this.in = new DataInputStream(in);
        this.out = new DataOutputStream(out);
    }

    /**
     * sends given string via output stream
     * @param str
     * @throws IOException
     */
    public void sendString(String str)
    {
        try {
            out.writeUTF(str);
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * waits till next message is received
     * @return
     */
    public String getCommand() {
        String str = null;
        try {
            str = in.readUTF();
        } catch (IOException e) {
            if (e.getMessage().equals("Socket closed"))
                Logger.getLogger(getClass().getName()).log(Level.INFO, e.getMessage());
            else
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return str;
    }

    /**
     * returns list of double points
     * @param str - expected to be in format x,x,x,x,x,x,x,x,x,x,
     * @return
     */
    public static List<double[]> getPoints(String str)
    {
        List<double[]> lst = new ArrayList<double[]>();
        String[] doubles = str.split("/");
        for (int i = 0; i < doubles.length;)
        {
            double x = Double.parseDouble(doubles[i++]);
            double y = Double.parseDouble(doubles[i++]);
            lst.add(new double[]{x, y});
        }
        return lst;
    }


    public void sendPoints(List<double[]> pts)
    {
        String str = "";
        for (int i = 0; i < pts.size();i++)
        {
            str += String.valueOf(pts.get(i)[0]);
            str += "/";
            str += String.valueOf(pts.get(i)[1]);
            str += "/";
        }
        sendString(Commands.POINTS.name() + ";" + str);
    }
    @Override
    public String toString()
    {
        return this.in.toString();
    }
}
