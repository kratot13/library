package main;

public enum WordsType{
    easy("/words/easy.txt"),
    hard("/words/hard.txt"),
    medium("/words/medium.txt"),
    objects("/words/objects.txt"),
    persons("/words/persons.txt"),
    verbs("/words/verbs.txt");

    private String filename;
    WordsType(String filename) {
        this.filename = filename;
    }

    public String getFilename()
    {
        return filename;
    }
}
