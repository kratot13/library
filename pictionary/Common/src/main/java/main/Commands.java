package main;

public enum Commands{
    NEW_ROOM,
    REMOVE_ROOM,
    JOIN_ROOM,
    LEAVE_ROOM,
    ROOM_CONFIRMED,
    ROOM_REJECTED,
    TRY_WORD,
    KICK_PLAYER,
    MESSSAGE,
    MOUSE_PRESSED,
    COLOR_CHANGED,
    TOO_MANY_ROOMS,
    TOO_MANY_PLAYERS,
    CLOSE_CONNECTION,
    TIME,
    CUR_WORD,
    ACTIVE,
    CURRENT_TURN,
    CORRECT,
    REQUEST_UPDATE_ROOMS,
    WRONG,
    RAISE_POINTS,
    PASSWORD,
    NEW_ID,
    POINTS,
    //used for joining room and creating one
    ALLOWED,
    DENIED,
    //
    PLAYERS_COUNT,
    NEXT_ROUND_IN,
    GAME_ENDED,
    REGISTER,
    LOGIN,
    ALERT,
    STATISTICS,
    LAST_WORD,
    ;

    public static String sendRoomConfirmed(int playerID, int roomID, int roomPassword){
        return ROOM_CONFIRMED.name() + ";" + playerID + ";" + roomID + ";" + roomPassword;
    }

    public static String sendNewRoom(boolean locked, int timelimit, int difficulty, int rounds)
    {
        return NEW_ROOM.name() + ";" + locked + ";" + timelimit + ";" + difficulty + ";" + rounds;
    }

    public static String sendMessage(int playerID, String playerNick, String messageText){
        return MESSSAGE.name() + ";" + playerID + ";" + playerNick + ";" + messageText;
    }

    public static String sendJoinRoom(int roomId)
    {
        return JOIN_ROOM.name() + ";" + roomId;
    }

    public static String sendJoinRoom(String password)
    {
        return JOIN_ROOM.name() + ";" + password;
    }

    public static String sendLeaveRoom()
    {
        return LEAVE_ROOM.name();
    }

    public static String sendTryWord(String word)
    {
        return TRY_WORD.name() + ";" + word;
    }

    public static String sendPlayerInit(int id, String nick){
        return id+ ";" + nick;
    }

    public static String sendRegister(String nick, String mail)
    {
        return REGISTER.name() + ";" + nick + ";" + mail;
    }

    public static String sendLogin(String nick, String pass)
    {
        return LOGIN.name() + ";" + nick + ";" + pass;
    }
}
