import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Tests
{
    private int a;
    private int b;
    public Tests()
    {
        a = 0;
        b = 0;
    }
    public static int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }
    public static void main(String[] args) throws IOException {
        /*
        Tests ts = new Tests();
        BufferedImage bimg = ImageIO.read(new File("words/test.jpg"));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write( bimg, "jpg", baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        ImageIcon imageIcon = new ImageIcon(imageInByte);
        Image img = imageIcon.getImage();

        BufferedImage bi = new BufferedImage(img.getWidth(null),img.getHeight(null),BufferedImage.TYPE_3BYTE_BGR);

        Graphics2D g2 = bi.createGraphics();
        g2.drawImage(img, 0, 0, null);
        g2.dispose();
        ImageIO.write(bi, "jpg", new File("words/img.jpg"));

        for (byte b : imageInByte) {
            System.out.println(b);
        }
        System.out.println("image size: " + imageInByte.length + " B");
        changeTests(ts, 5, 5);

        System.out.println(ts.toString());
        */
        /*
        double[][] pts = new double[5][2];
        for(int i = 0; i < pts.length; i++)
        {
            pts[i][0] = i;
            pts[i][1] = (double)i / 2;
        }

        for(int i = 0; i < pts.length; i++)
        {
            for(int j = 0; j < pts[i].length; j++)
            {
                System.out.println(pts[i][j]);
            }
        }

         */
        int b = 0;
        try {
            String a = "1y2";
            b = Integer.parseInt(a);
        } catch (Exception e)
        {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        System.out.println(b);
    }

    static void changeTests(Tests ts, int a, int b)
    {
        ts.a = a;
        ts.b = b;
    }

    public String toString()
    {
        return Integer.toString(a) + ", " + Integer.toString(b);
    }
}
