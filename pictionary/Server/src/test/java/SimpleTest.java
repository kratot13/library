import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleTest {
    public static void main(String[] args) throws Exception {
        Server s = Server.getInstance();
        Database d = Database.getInstance();
        //d.registerUser("Tomicek", "kratochviltom00@gmail.com");
/*
        if (!d.resetPassword("Tomas"))
        {
            System.out.println("password reset FAILED");
        }
        else
            System.out.println("password reset succeded");*/
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest("asdf".getBytes(StandardCharsets.UTF_8));
        for (int i = 0; i < hash.length; i++) {
            System.out.printf("%02X", hash[i]);
        }
    }

    static String request() throws Exception
    {
        URL url = new URL("http://training-diary.000webhostapp.com/Pictionary/login.php");
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        params.put("nick", "Tomik");
        params.put("pass", "123456");

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

        String inputLine = null;
        StringBuilder res =  new StringBuilder();
        while ((inputLine = ((BufferedReader) in).readLine()) != null)
            res.append(inputLine + "\n");
        in.close();
        return res.length() == 0 ? null : res.toString();
    }

    static String post(String urlParameters)
    {
        HttpURLConnection connection = null;

        try {
            //Create connection
            URL url = new URL("http://training-diary.000webhostapp.com/Pictionary/testing.php");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length",
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(url.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                System.out.println(inputLine);
            in.close();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
