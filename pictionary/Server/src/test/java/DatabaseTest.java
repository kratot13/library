import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.junit.Assert;
import org.junit.Test;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DatabaseTest {

    private static final Database dtb = Database.getInstance();


    @Test
    public void requestShouldReturnPageContent(){
        final String expected = "Testing successful!";
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        String value = "";
        try {
            value = dtb.request(params, "testing.php");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(expected, value);
    }
    @Test
    public void wrongPhpShouldThrowIOException(){
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        String value = "";
        boolean exThrown = false;
        try {
            value = dtb.request(params, "nonexisting.php");
        } catch (IOException e) {
            assertEquals(Integer.MAX_VALUE, Integer.MAX_VALUE);
            exThrown = true;
        }
        if (!exThrown)
            assertEquals(Integer.MAX_VALUE, Integer.MIN_VALUE);
    }

    @Test
    public void getStatisticsShouldReturnMapOfStats(){
        Player pl = new Player(null);
        pl.setNick("Tomicek");
        pl.setPassword("123456");
        Map<String,Object> value = dtb.getStatistics(pl);
        Integer.parseInt((String) value.get("score"));
        Integer.parseInt((String) value.get("games_played"));
    }

    @Test
    public void correctEmailShouldReturnTrue(){
        final boolean expected = isValidEmailAddress("mymail@mymail.com");
        final boolean value = true;
        assertEquals(expected, value);
    }

    @Test
    public void wrongEmailShouldReturnFalse(){
        final boolean expected = isValidEmailAddress("mymail,@mymail.com");
        final boolean value = false;
        assertEquals(expected, value);
    }

    //copied from Database
    private boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
}
