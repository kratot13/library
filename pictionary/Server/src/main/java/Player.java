import main.Commands;
import main.TCPsocket;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Player extends Thread
{
    public static final int ID_LENGTH = 9;
    public static final List<Player> PLAYER_LIST = Collections.synchronizedList(new ArrayList<Player>());
    private Socket s = null;
    private TCPsocket tcp;
    private int id;
    private String nick;
    private int score;
    private int games_played;
    private String password;
    private Room room;
    private boolean active;
    private boolean guessCorrect;
    private boolean online;

    public static final int GUESSED_SCORE = 5;
    public static final int DREW_SCORE = 2;


    public Player(Socket s) {
        System.out.println("open: " + s);
        this.s = s;
        try {
            if (s != null)
                this.tcp = new TCPsocket(s.getInputStream(), s.getOutputStream());
        } catch (IOException e) {
            online = false;
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        this.score = 0;
        active = false;
        online = true;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(List<Player> players, boolean active) {
        this.active = active;
        for (Player pl: players)
        {
            pl.tcp.sendString(Commands.ACTIVE.name() + ";" + pl.getID());
        }
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public int getID() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGames_played() {
        return games_played;
    }

    public void setGames_played(int games_played) {
        this.games_played = games_played;
    }

    /**
     * main thread for receiving commands
     */
    @Override
    public void run() {
        while(online)
        {
            String str;

            if ((str = tcp.getCommand()) == null){
                break;
            }
            System.out.println(str);
            if (!actionByCommand(str)) continue;

            if (room == null) continue;
            List<Player> players = room.getPlayers();
            synchronized (room.getPlayers())
            {
                for (int i = 0; i < players.size(); i++)
                {
                    if (players.get(i) != this)
                    {
                        players.get(i).tcp.sendString(str);
                    }
                }
            }
        }

        System.out.println("close: " + this.tcp);
        if (room != null)
        {
            leaveRoomHandle();
            synchronized (PLAYER_LIST)
            {
                PLAYER_LIST.remove(this);
            }
        }
        try {
            this.s.close();
        } catch (IOException ex) {
            online = false;
            Logger.getGlobal().log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getNick() {
        return nick;
    }

    public int getScore() {
        return score;
    }

    /**
     * do what paramater commands
     * @param command
     * @return
     */
    private synchronized boolean actionByCommand(String command) {
        String[] params = command.split(";");
        Commands cmd = null;
        try {
            cmd = Commands.valueOf(params[0]);
        }
        catch(Exception e){
            online = false;
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            return false;
        }
        boolean ret = true;
        switch (cmd)
        {
            case STATISTICS:
            {
                if (params.length == 1 && this.password != null){
                    Map<String,Object> stats = Database.getInstance().getStatistics(this);
                    this.tcp.sendString(Commands.STATISTICS.name() + ";" +
                            (String)stats.get("score") + ";" +
                            (String)stats.get("games_played"));
                }
                else
                    this.tcp.sendString(Commands.ALERT.name() + ";Could not fetch statistics");
                break;
            }
            case LOGIN:
            {
                if (params.length == 3 && Database.getInstance().login(this, params[1], params[2])){
                    this.tcp.sendString(Commands.ALERT.name() + ";User logged in.");
                    this.tcp.sendString(Commands.LOGIN.name() + ";" + getNick());
                }
                else
                    this.tcp.sendString(Commands.ALERT.name() + ";Wrong initials.");
                break;
            }
            case REGISTER:
            {
                if (params.length == 3 && Database.getInstance().registerUser(params[1], params[2]))
                    this.tcp.sendString(Commands.ALERT.name() + ";User registered, confirm your email.");
                else
                    this.tcp.sendString(Commands.ALERT.name() + ";This email or nick could not be registered.");
                break;
            }
            case NEW_ROOM:
            {
                boolean locked = Boolean.parseBoolean(params[1]);
                int timeLimit = Integer.parseInt(params[2]);
                int difficulty = Integer.parseInt(params[3]);
                int rounds = Integer.parseInt(params[4]);
                Room room = Server.getInstance().addRoom(locked, timeLimit, difficulty, rounds);
                if (room == null)
                {
                    this.tcp.sendString(Commands.DENIED.name());
                    break;
                }
                this.tcp.sendString(Commands.ALLOWED.name());
                this.tcp.sendString(Commands.PASSWORD + ";" + room.getPassword());
                boolean done = Server.getInstance().joinRoom(room.getID(), this);
                ret = false;
                break;
            }
            case JOIN_ROOM:
            {
                int id = Integer.parseInt(params[1]);
                if (params[1].length() == Room.PASSWORD_LENGTH)
                {
                    boolean done = Server.getInstance().joinRoomByPassword(id, this);
                }
                else
                {
                    boolean done = Server.getInstance().joinRoom(id, this);
                }
                ret = false;
                break;
            }
            case LEAVE_ROOM:
            {
                leaveRoomHandle();
                ret = false;
                break;
            }
            case TRY_WORD:
            {
                if (room == null)
                {
                    this.tcp.sendString("EXCEPTION - trying word in null room");
                    break;
                }
                tryWordStatus(params[1]);
                break;
            }
            case REQUEST_UPDATE_ROOMS:
            {
                Room[] rooms = Server.getInstance().getRooms();
                for (Room room : rooms)
                {
                    tcp.sendString(Commands.sendNewRoom(room.isLocked(), room.getID(), room.getDifficulty(), room.getRounds()));
                }
                break;
            }
            case CLOSE_CONNECTION:
            {
                online = false;
                ret = false;
                break;
            }
            default:
            {
                return ret;
            }
        }
        return ret;
    }

    /**
     * do whatever is necessary to leave the room
     */
    public void leaveRoomHandle()
    {
        this.setScore(0);
        room.removePlayer(this);
        if (room.getPlayerCount() < 2)
            room.setActive(false);
        if (room.getPlayerCount() == 0)
        {
            for(Player pl : PLAYER_LIST)
            {
                pl.tcp.sendString(Commands.REMOVE_ROOM.name() + ";" + room.getID());
            }
            System.out.println("room removed!");
            Server.getInstance().getRooms()[room.getID()] = null;
            room = null;
        }
    }

    private void tellOthersPlayerGuessedCorrectly(Player drawingPlayer) {
        synchronized (PLAYER_LIST)
        {
            for(Player pl : this.room.getPlayers())
            {
                pl.tcp.sendString(Commands.RAISE_POINTS.name() + ";" + id + ";" + score);
                pl.tcp.sendString(Commands.RAISE_POINTS.name() + ";" + drawingPlayer.getID() + ";" + drawingPlayer.getScore());
            }
        }
    }

    private void tryWordStatus(String word) {
        if (word.equals(room.getCurWord()) && !guessCorrect)
        {
            this.score += GUESSED_SCORE;
            Database.getInstance().setStats(this, GUESSED_SCORE, 0);
            Player drawingPl =  room.getPlayers().get(room.getTurn());
            drawingPl.setScore(drawingPl.getScore() + DREW_SCORE);
            Database.getInstance().setStats(drawingPl, DREW_SCORE, 0);
            this.tcp.sendString(Commands.CORRECT.name() + ";" + this.score);
            tellOthersPlayerGuessedCorrectly(drawingPl);
            room.playerGuessedCorrecly();
            guessCorrect = true;
        }
        else if (!guessCorrect)
            this.tcp.sendString(Commands.WRONG.name());
    }

    public void setOnline(boolean value) { this.online = value; }

    public TCPsocket getTcp() {
        return tcp;
    }

    public void newRound()
    {
        guessCorrect = false;
    }
}