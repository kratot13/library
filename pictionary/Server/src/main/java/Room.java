import main.Commands;
import main.WordsType;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krata
 */
public class Room implements Runnable
{
    public static final int PASSWORD_LENGTH = 8;
    private final int SLEEP_TIME = 1000;
    private final int NEXT_ROUND_WAIT = 5;
    private final int GAME_END_TIME = 15;
    private final int MAX_PLAYERS = 6;
    private boolean locked;
    private int id;
    private int difficulty;
    private int rounds;
    private List<Player> players;
    private int playersGuessedCorrecly;
    private boolean gameRunning;
    private short turn;
    private String curWord;
    private BufferedImage img;
    private WordsType wt;
    private int timeLimit;
    private int password;
    private boolean active;
    private Thread thread;
    private int currentRound;

    public Room(boolean locked, int id, int timeLimit, int difficulty, int rounds) {
        this.locked = locked;
        this.id = id;
        this.difficulty = difficulty;
        this.rounds = rounds;
        players = Collections.synchronizedList(new ArrayList<Player>());
        players = new ArrayList<Player>();
        wt = getWordsType(difficulty);
        this.timeLimit = timeLimit;
        turn = -1;
        System.out.println("Room #" + this.id);
        if(!locked)
        {
            for(Player pl : Player.PLAYER_LIST)
            {
                pl.getTcp().sendString(Commands.sendNewRoom(isLocked(), id, difficulty, rounds));
            }
        }
        //this.start();
        this.currentRound = 0;
    }

    private WordsType getWordsType(int difficulty)
    {
        switch (difficulty)
        {
            case 3:
                return WordsType.objects;
            case 5:
                return WordsType.verbs;
            case 4:
                return WordsType.hard;
            case 2:
                return WordsType.medium;
            case 1:
            default:
                return WordsType.easy;
        }
    }
    private void startGame(){
        long beforeTime = System.currentTimeMillis();
        int curSec = 0;
        while(curSec < NEXT_ROUND_WAIT)
        {
            long diff = System.currentTimeMillis() - beforeTime;
            curSec = (int)diff / 1000;
            String str = Commands.NEXT_ROUND_IN.name() + ";"  + (NEXT_ROUND_WAIT - curSec) + ";" + (currentRound + 1);
            for (Player pl : players)
            {
                pl.getTcp().sendString(str);
            }
            System.out.println(str);
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
        }
        playersGuessedCorrecly = 0;
        img = null;
        try {
            curWord = getRandomWord(wt);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        if (!players.isEmpty())
            players.get(getTurn()).getTcp().sendString(Commands.CUR_WORD.name() + ";" + curWord);
        else
            return;
        StringBuilder curWordLength = new StringBuilder();
        for (int i = 0; i < curWord.length(); i++)
        {
            curWordLength.append(curWord.charAt(i) != ' ' ? "." : " ");
        }
        for (Player pl : players)
        {
            if (pl != players.get(getTurn()))
            {
                pl.getTcp().sendString(Commands.CUR_WORD.name() + ";"  + curWordLength.toString());
            }
        }
        System.out.println(curWord);
        curSec = 0;
        beforeTime = System.currentTimeMillis();
        while(curSec < timeLimit && active && getPlayerCount() - 1 != playersGuessedCorrecly)
        {
            long diff = System.currentTimeMillis() - beforeTime;
            curSec = (int)diff / 1000;
            for (Player pl : players)
            {
                pl.getTcp().sendString(Commands.TIME.name() + ";"  + (timeLimit - curSec));
            }
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    public boolean isGameRunning() {
        return gameRunning;
    }

    public int getMAX_PLAYERS() {
        return MAX_PLAYERS;
    }

    public int getID() {
        return id;
    }

    private void setNewRound()
    {
        for (Player pl : players)
        {
            pl.newRound();
        }
    }

    /**
     * game thread
     * function calls startGame()
     * when game finishes, the winner is selected
     * and after some time, everyone leaves the room
     **/
    public void run() {
        while(getPlayerCount() >= 2 && currentRound < getRounds())
        {
            if (gameRunning) break;
            setNewRound();
            active = true;
            short turn = getTurn();
            turn++;
            if (turn >= getPlayerCount())
                turn = 0;
            setTurn(turn);
            gameRunning = true;
            startGame();
            gameRunning = false;
            for(Player pl : players)
            {
                if (!pl.isActive()) {
                    pl.setActive(players, true);
                }
                Database.getInstance().setStats(pl, 0, 1);
                pl.getTcp().sendString(Commands.LAST_WORD.name() + ";" + curWord);
            }
            currentRound++;
        }
        if (currentRound < getRounds())
            return;
        //Server.getInstance().getRooms()[id] = null;
        String winnerName = getWinner().getNick();
        for(Player pl : players)
        {
            pl.setScore(0);
            pl.getTcp().sendString(Commands.GAME_ENDED.name() + ";" + winnerName);
        }
        long beforeTime = System.currentTimeMillis();
        int curSec = 0;
        while(curSec < GAME_END_TIME)
        {
            long diff = System.currentTimeMillis() - beforeTime;
            curSec = (int)diff / 1000;
            for (Player pl : players)
            {
                pl.getTcp().sendString(Commands.TIME.name() + ";" + (GAME_END_TIME - curSec));
            }
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
        }

        ListIterator<Player> iterator = players.listIterator();
        while (iterator.hasNext()) {
            Player pl = iterator.next();
            pl.getTcp().sendString(Commands.LEAVE_ROOM.name());
            pl.leaveRoomHandle();
        }
    }

    private Player getWinner()
    {
        int max = -1;
        Player winner = null;
        for(Player pl : players)
        {
            if (pl.getScore() > max)
            {
                max = pl.getScore();
                winner = pl;
            }
        }
        return winner;
    }

    /**
     * removes given player from player list
     * @param pl
     * @return
     */
    public boolean removePlayer(Player pl){
        tellOthersPlayerLeft(pl);
        players.remove(pl);
        return true;
    }

    private void tellOthersPlayerLeft(Player p) {
        for(Player pl : players)
        {
            if (pl != p)
                pl.getTcp().sendString(Commands.LEAVE_ROOM.name() + ";" + p.getID());
        }
    }

    private void tellOthersPlayerCorrect(int id) {
        for(Player pl : players)
        {
            if (pl.getID() != id)
                pl.getTcp().sendString(Commands.CORRECT.name() + ";" + id);
        }
    }



    /**
     * sends list of all players in the room to player
     * @param pl
     */
    private void sendRoomPlayersInfo(Player pl)
    {
        for (Player p : players)
        {
            pl.getTcp().sendString(Commands.JOIN_ROOM.name() + ";" + p.getID() + ";" + p.getNick() + ";" + p.getScore());
        }
        for (Player p : Player.PLAYER_LIST)
        {
            p.getTcp().sendString(Commands.PLAYERS_COUNT.name() + ";" + id + ";" + getPlayerCount());
        }
    }

    private void updatePlayersAboutNewPlayer(Player newPlayer){
        for (Player p : players)
        {
            p.getTcp().sendString(Commands.JOIN_ROOM.name() + ";" + newPlayer.getID() + ";" + newPlayer.getNick() + ";" + newPlayer.getScore());
        }
    }

    /**
     * adds player to list of players if possible
     * if there is 2 players, new game is started
     * @param pl
     * @return
     */
    public boolean addPlayer(Player pl)
    {
        if (getPlayers().size() >= MAX_PLAYERS){
            //when there is full capacity of room, player is rejected
            pl.getTcp().sendString(Commands.DENIED.name());
            return false;
        }
        //confirm, that player can play
        pl.getTcp().sendString(Commands.sendRoomConfirmed(pl.getID(), id, getPassword()));
        //says all players except the new one, that another player has joined the room
        updatePlayersAboutNewPlayer(pl);
        pl.setRoom(this);
        players.add(pl);
        //all players in current room are sent to new player
        sendRoomPlayersInfo(pl);
        if (getPlayerCount() == 2 && !gameRunning)
        {
            thread = new Thread(this);
            thread.start();
        }
        return true;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public WordsType getWt() {
        return wt;
    }

    public void setWt(WordsType wt) {
        this.wt = wt;
    }

    public short getTurn() {
        return turn;
    }

    public void setTurn(short turn) {
        this.turn = turn;
        for(Player pl : players)
        {
            pl.getTcp().sendString(Commands.CURRENT_TURN.name() + ";" + getPlayers().get(getTurn()).getID());
        }
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public void playerGuessedCorrecly()
    {
        playersGuessedCorrecly++;
    }

    public String getCurWord() {
        return curWord;
    }

    public void setCurWord(String curWord) {
        this.curWord = curWord;
    }

    public BufferedImage getImg() {
        return img;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public int getPlayerCount() {
        return players.size();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private String getRandomWord(WordsType wt) throws IOException {
        InputStream is = getClass().getResourceAsStream(wt.getFilename());
        BufferedReader in = new BufferedReader(new InputStreamReader(is));

        String inputLine;
        List<String> words = new ArrayList<String>();
        while ((inputLine = in.readLine()) != null)
            words.add(inputLine);
        in.close();
        return words.get(new Random().nextInt(words.size()));
    }

}
