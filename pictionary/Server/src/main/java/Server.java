import main.Commands;

import java.io.IOException;
import java.net.*;
import java.util.Enumeration;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server
{
    private final int MAX_ROOMS = 100;
    private Room[] rooms;
    private int roomCount;
    private ServerSocket serverSocket;

    // static variable single_instance of type Singleton
    public static Server single_instance = null;

    // static method to create instance of Singleton class
    public static Server getInstance()
    {
        if (single_instance == null) {
            try {
                single_instance = new Server(new ServerSocket(2343));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return single_instance;
    }

    public ServerSocket getsSocket(){
        return this.serverSocket;
    }

    private Server(ServerSocket serverSocket)
    {
        this.serverSocket = serverSocket;
        roomCount = 0;
        rooms = new Room[MAX_ROOMS];
        addRoom(false, 10, 10, 10);
    }


    private void run() {
        System.out.println("listen: ");
        Enumeration<NetworkInterface> e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage(), ex);
        }
        while (e.hasMoreElements()) {
            Enumeration<InetAddress> ee = e.nextElement().getInetAddresses();
            while (ee.hasMoreElements()) {
                System.out.println("   " + ee.nextElement().getHostAddress());
            }
        }


        while (true)
        {
            Socket s = null;
            try {
                s = serverSocket.accept();
            } catch (IOException ex) {
                Logger.getGlobal().log(Level.WARNING, ex.getMessage(), ex);
            }
            Player pl = new Player(s);
            String params = pl.getTcp().getCommand();
            changePlayer(pl, params);
            Player.PLAYER_LIST.add(pl);
            pl.start();
        }
    }

    private void changePlayer(Player pl, String params) {
        System.out.println(params);
        String[] param = params.split(";");
        int id = Integer.parseInt(param[0]);
        id = id == -1 ? getNewPlayerId() : id;
        pl.getTcp().sendString(Commands.NEW_ID.name() + ";" + id);
        pl.setId(id);
        pl.setNick(param[1]);
        for(Room room : getRooms())
        {
            if (room != null)
                pl.getTcp().sendString(Commands.sendNewRoom(room.isLocked(), room.getID(), room.getDifficulty(), room.getRounds()));
        }
        //throw new NotImplementedException();
    }

    private int getNewPlayerId()
    {
        return new Random().nextInt(Integer.MAX_VALUE);
    }

    public Room[] getRooms()
    {
        return rooms;
    }

    /**
     * player joins defined room
     * @param roomId
     * @param player
     * @return
     */
    public boolean joinRoom(int roomId, Player player)
    {
        if (roomId >= MAX_ROOMS || rooms[roomId] == null)
        {
            player.getTcp().sendString(Commands.DENIED.name());
            return false;
        }
        return rooms[roomId].addPlayer(player);
    }

    /**
     * player joins room by key
     * @param password
     * @param player
     * @return
     */
    public boolean joinRoomByPassword(int password, Player player)
    {
        for (Room room : Server.getInstance().getRooms())
        {
            if (room != null && room.getPassword() == password)
                return room.addPlayer(player);
        }
        player.getTcp().sendString(Commands.DENIED.name());
        return false;
    }

    private boolean removeRoom(int id)
    {
        if (id >= MAX_ROOMS || rooms[id] == null) return false;
        rooms[id] = null;
        return true;
    }


    /**
     * adds room with given parameters
     * @param locked
     * @param timeLimit
     * @param difficulty
     * @param rounds
     * @return
     */
    public Room addRoom(boolean locked, int timeLimit, int difficulty, int rounds)
    {
        int id = newId();
        if (id == -1) return null;
        Room room = new Room(locked, id, timeLimit, difficulty, rounds);
        room.setPassword(newPassword());
        rooms[id] = room;
        return room;
    }

    private int newPassword()
    {
        int a = 0;
        Random rnd = new Random();
        for (int i = 0; i < Room.PASSWORD_LENGTH - 1; i++)
        {
            a += rnd.nextInt(10) * Math.pow(10, i);
        }
        a += (rnd.nextInt(9) + 1) * Math.pow(10, Room.PASSWORD_LENGTH - 1);
        return a;
    }

    private int newId()
    {
        for (int i = 0; i < MAX_ROOMS; i++)
        {
            if (rooms[i] == null) return i;
        }
        return -1;
    }

    /**
     * server starts here
     * @param args
     */
    public static void main(String[] args) {
        Server server = getInstance();
        server.run();
    }


}
