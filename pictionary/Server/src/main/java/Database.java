import javax.mail.*;
import javax.mail.PasswordAuthentication;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.server.ExportException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Database
{
    private final String addr = "http://training-diary.000webhostapp.com/Pictionary/";
    private final String hrefLink = "<a href=\"%s\">New password</a>";
    private final String newPassAddr = "http://training-diary.000webhostapp.com/?key=";
    private final String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private final String senderEmail = "pictionary@seznam.cz";//change with your sender email
    private final String senderPassword = "Heslo123";//change with your sender password

    private boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    /**
     * sends en email from logged account
     * @param to
     * @param title
     * @param html
     */
    public void sendAsHtml(String to, String title, String html) {
        //if (title.equals("New password")) return;
        System.out.println("Sending email to " + to);

        Session session = createSession();

        //create message using session
        MimeMessage message = new MimeMessage(session);
        try {
            prepareEmailMessage(message, to, title, html);
            //sending message
            Transport.send(message);
        } catch (MessagingException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        System.out.println("Done");
    }

    private void prepareEmailMessage(MimeMessage message, String to, String title, String html)
            throws MessagingException {
        message.setContent(html, "text/html; charset=utf-8");
        message.setFrom(new InternetAddress(senderEmail));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        message.setSubject(title);
    }

    private Session createSession() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");//Outgoing server requires authentication
        props.put("mail.smtp.starttls.enable", "true");//TLS must be activated
        props.put("mail.smtp.host", "smtp.seznam.cz"); //Outgoing server (SMTP) - change it to your SMTP server
        props.put("mail.smtp.port", "25");//Outgoing port

        return Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });
    }

    /**
     * gets user's email by nick which is the unique key
     * creates random key which is required to change the password
     * then an email is sent with the key
     * @param nick
     * @return
     */
    public boolean resetPassword(String nick) {
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        params.put("nick", nick);
        String email = null;
        try {
            email = request(params, "fetchEmail.php");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (email == null)
            return false;
        Random rnd = new Random();
        StringBuilder newPass = new StringBuilder();
        for (int i = 0; i < 16; i++)
        {
            newPass.append(getRandomChar(rnd));
        }
        System.out.println("heslo: " + newPass);
        params = new LinkedHashMap<String, Object>();
        params.put("pass", newPass.toString());
        params.put("nick", nick);
        String res = null;
        try {
            res = request(params, "resetPassword.php");
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        if (res == null)
            return false;
        Logger.getGlobal().log(Level.INFO, res);
        sendAsHtml(email, "New password", String.format(hrefLink, newPassAddr + newPass.toString()));
        return true;
    }

    /**
     * basic addition, server takes the given values
     * and adds them to current database values
     * @param pl
     * @param score
     * @param gameEnd
     * @return
     */
    public boolean setStats(Player pl, int score, int gameEnd)
    {
        if (pl.getPassword() == null)
            return false;
        final String php = "updateStats.php";
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        params.put("score", score);
        params.put("nick", pl.getNick());
        params.put("pass", pl.getPassword());
        params.put("games_played", gameEnd);
        String res = null;
        try {
            res = request(params, php);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        if (res == null)
            return false;
        return true;
    }

    /**
     * gets player's score and how many games they played
     * @param pl
     * @return map
     */
    public Map<String,Object> getStatistics(Player pl)
    {
        final String php = "login.php";
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        params.put("pass", pl.getPassword());
        params.put("nick", pl.getNick());
        String res = null;
        try {
            res = request(params, php);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        if (res == null)
            return null;
        params = new LinkedHashMap<String, Object>();
        String[] param = res.split(" ");
        params.put("score", param[0]);
        params.put("games_played", param[1]);
        //pl.setScore(Integer.parseInt(param[0]));
        pl.setGames_played(Integer.parseInt(param[1]));
        return params;
    }

    /**
     * sets player's nick and password for future uses
     * @param pl
     * @param nick
     * @param pass
     * @return
     */
    public boolean login(Player pl, String nick, String pass)
    {
        final String registerPHP = "login.php";
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        params.put("pass", pass);
        params.put("nick", nick);
        String res = null;
        try {
            res = request(params, registerPHP);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        if (res == null)
            return false;
        pl.setNick(nick);
        pl.setPassword(pass);
        String[] param = res.split(" ");
        //pl.setScore(Integer.parseInt(param[0]));
        pl.setGames_played(Integer.parseInt(param[1]));
        return true;
    }

    /**
     * creates new row in database with given nick and email
     * then the resetPassword function is called
     * @param nick
     * @param email
     * @return
     */
    public boolean registerUser(String nick, String email)
    {
        final String registerPHP = "register.php";
        Map<String,Object> params = new LinkedHashMap<String, Object>();
        params.put("email", email);
        params.put("nick", nick);
        if (!isValidEmailAddress(email))
            return false;
        String res = null;
        try {
            res = request(params, registerPHP);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        if (res == null)
            return false;
        System.out.println("confirm email");
        resetPassword(nick);
        return true;
    }

    /**
     * sends POST request with given parameters
     * @param params
     * @param php
     * @return
     * @throws IOException
     */
    public String request(Map<String, Object> params, String php) throws IOException {
        URL url = new URL(addr + php);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),
                StandardCharsets.UTF_8));

        String inputLine = "";
        StringBuilder res =  new StringBuilder();
        while ((inputLine = in.readLine()) != null)
            res.append(inputLine);
        in.close();
        return res.length() == 0 ? null : res.toString();
    }

    // static variable single_instance of type Singleton
    public static Database single_instance = null;

    private Database()
    {

    }

    // static method to create instance of Singleton class
    public static Database getInstance()
    {
        if (single_instance == null)
            single_instance = new Database();

        return single_instance;
    }

    private char getRandomChar(Random rnd)
    {
        return alphabet.charAt(rnd.nextInt(alphabet.length()));
    }
}
