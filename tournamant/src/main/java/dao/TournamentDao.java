package dao;

import model.Club;
import model.Tournament;

import javax.persistence.EntityManager;
import java.util.List;

public class TournamentDao {
    private EntityManager em;

    public TournamentDao() {
    }

    public TournamentDao(EntityManager em) {
        this.em = em;
    }

    public List<Tournament> findAll(){
        return em.createQuery("SELECT t FROM Tournament t", Tournament.class).getResultList();
    }

    public void create(Tournament t){
        em.persist(t);
    }
}
