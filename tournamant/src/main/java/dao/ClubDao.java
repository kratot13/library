package dao;


import model.Club;

import javax.persistence.EntityManager;
import java.util.List;

public class ClubDao {
    private EntityManager em;

    public ClubDao() {
    }

    public ClubDao(EntityManager em) {
        this.em = em;
    }

    public List<Club> findAll(){
        return em.createQuery("select t from Club t", Club.class).getResultList();
    }

    public void create(Club c){
        em.persist(c);
    }

}
