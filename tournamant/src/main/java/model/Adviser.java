package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Adviser {
    @Id
    @GeneratedValue
    private String nick;
    private int number;
    private String organization;

    @ManyToMany(mappedBy = "adviserList")
    private List<Tournament> tournamentList;

    public Adviser() {
        this.tournamentList = new ArrayList<>();
    }

    @Override
    public String toString(){
        return nick;
    }

    public int getNumber(){
        return number;
    }
}

