package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RefereeCard {
    private String forename;
    private String surname;
    @Id
    private int number;
    @Id
    private String organization;

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }
}
