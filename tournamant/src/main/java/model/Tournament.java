package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Tournament {
    @Id
    @GeneratedValue
    private String name;

    private Date date;
    private String league;

    @Column(unique = true)
    private String club_name;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<Adviser> adviserList;
    public Tournament(){
        adviserList = new ArrayList<>();
    }

    public List<Adviser> getAdviserList() {
        return adviserList;
    }

    public void setAdviserList(List<Adviser> adviserList) {
        this.adviserList = adviserList;
    }

    public String getAdvisors() {
        return advisorsToString();
    }

    public String advisorsToString(){
        String prefix = "";
        StringBuilder ret = new StringBuilder();
        for(Adviser a : adviserList){
            ret.append(prefix);
            prefix = ",";
            ret.append(a);
        }
        return ret.toString();
    }

    public boolean canRefereeEdit(int id){
        for (Adviser adv : adviserList){
            if (id == adv.getNumber())return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getClub_name() {
        return club_name;
    }

    public void setClub_name(String club) {
        this.club_name = club;
    }

    public void create(){

    }
}
