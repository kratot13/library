package sample;

import controllers.ScreenController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Tournament manager");
        ScreenController.getInstance().addScreen("addTour", FXMLLoader.load(getClass().getResource("/add_tour.fxml")));
        primaryStage.setScene(ScreenController.getInstance().activate("addTour"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
