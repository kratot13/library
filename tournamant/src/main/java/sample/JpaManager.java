package sample;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaManager {

    private static JpaManager single_instance = null;
    private EntityManagerFactory emf;
    private EntityManager em;

    private JpaManager(){
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }

    public EntityManagerFactory getEmf() {
        return emf;
    }

    public EntityManager getEm() {
        return em;
    }

    public static JpaManager getInstance(){
        if (single_instance == null){
            single_instance = new JpaManager();
        }
        return single_instance;
    }
}
