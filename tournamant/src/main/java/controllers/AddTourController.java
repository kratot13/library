package controllers;

import dao.ClubDao;
import dao.TournamentDao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Adviser;
import model.Club;
import model.Tournament;
import sample.JpaManager;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class AddTourController {
    @FXML
    Button btnAdd;
    @FXML
    Button btnEdit;
    @FXML
    Button btnAddAdvisor;
    @FXML
    TextField fieldName;
    @FXML
    TextField fieldId;
    @FXML
    ChoiceBox<String> comboLeague;
    @FXML
    ChoiceBox<String> comboClub;
    @FXML
    DatePicker datePicker;
    @FXML
    TableView<Tournament> tourTable;

    private Tournament selectedTour = null;
    public void AddTourClicked(ActionEvent event) {
        Tournament t = checker();
        if (t == null) return;
        EntityManager em =JpaManager.getInstance().getEm();
        List<Adviser> lst = em.createQuery("SELECT t FROM Adviser t WHERE t.number=:p", Adviser.class)
                            .setParameter("p",  Integer.parseInt(fieldId.getText())).getResultList();
        if (lst.size() == 0){
            messageBox("An exception", "Referee does not exist",Alert.AlertType.ERROR);
            return;
        }
        t.setAdviserList(lst);
        try{
            JpaManager.getInstance().getEm().persist(t);
            loadTours();
        }
        catch (Exception e){
            if (e.getMessage().contains("already exists")){
                messageBox("An exception", "This tournament name is taken",Alert.AlertType.ERROR);
                JpaManager.getInstance().getEm().getTransaction().rollback();
                loadTours();
                return;
            }
        }
    }

    public void EditTourClicked(ActionEvent event){
        if (selectedTour == null) {
            messageBox("Warning", "Select a tournament first",Alert.AlertType.WARNING);
            return;
        }
        Tournament t = checker();
        if (t == null)return;
        if (!t.getName().equals(selectedTour.getName())){
            messageBox("An exception", "You can edit only tournaments with same name",Alert.AlertType.ERROR);
            return;
        }
        if (!selectedTour.canRefereeEdit(Integer.parseInt(fieldId.getText()))){
            messageBox("An exception", "Only advisors of this tournament can edit",Alert.AlertType.ERROR);
            return;
        }
        t.setAdviserList(selectedTour.getAdviserList());
        JpaManager.getInstance().getEm().merge(t);
        loadTours();
    }

    public void DeleteTourClicked(ActionEvent event){
        Tournament t = checker();
        if (t == null)return;
        if (!selectedTour.canRefereeEdit(Integer.parseInt(fieldId.getText()))){
            messageBox("An exception", "Only advisors of this tournament can delete tournament",Alert.AlertType.ERROR);
            return;
        }
        JpaManager.getInstance().getEm().remove(selectedTour);
        selectedTour = null;
        loadTours();
    }

    public void AddRefClicked(ActionEvent event){
        if (selectedTour == null) {
            messageBox("Warning", "Select a tournament first",Alert.AlertType.WARNING);
            return;
        }
        if (fieldId.getText().length() == 0 || !fieldId.getText().matches("\\d*") ||
                !selectedTour.canRefereeEdit(Integer.parseInt(fieldId.getText()))){
            messageBox("An exception", "Only advisors of this tournament can add referees tournament",Alert.AlertType.ERROR);
            return;
        }
        TextInputDialog td = new TextInputDialog("");
        td.setHeaderText("Enter the refeere ID you want to add");
        Optional<String> result = td.showAndWait();
        if (!result.isPresent()) return;
        List<Adviser> lst = JpaManager.getInstance().getEm().createQuery("SELECT t FROM Adviser t WHERE t.number=:p", Adviser.class)
                .setParameter("p",  Integer.parseInt(result.get())).getResultList();
        if (lst.isEmpty()) {
            messageBox("An exception", "This referee does not exist",Alert.AlertType.ERROR);
            return;
        }
        if (selectedTour.canRefereeEdit(lst.get(0).getNumber())) {
            messageBox("An exception", "This referee already advices this tournament",Alert.AlertType.ERROR);
            return;
        }
        selectedTour.getAdviserList().add(lst.get(0));
        JpaManager.getInstance().getEm().merge(selectedTour);
        loadTours();
    }

    public void RemoveRefClicked(ActionEvent event){
        if (selectedTour == null) {
            messageBox("Warning", "Select a tournament first",Alert.AlertType.WARNING);
            return;
        }
        if (selectedTour.getAdviserList().size() == 1){
            messageBox("An exception", "Tournament must have at least one referee",Alert.AlertType.ERROR);
            return;
        }
        if (fieldId.getText().length() == 0 || !fieldId.getText().matches("\\d*") ||
                !selectedTour.canRefereeEdit(Integer.parseInt(fieldId.getText()))){
            messageBox("An exception", "Only advisors of this tournament can remove referees tournament",Alert.AlertType.ERROR);
            return;
        }
        TextInputDialog td = new TextInputDialog("");
        td.setHeaderText("Enter the refeere ID you want to remove");
        Optional<String> result = td.showAndWait();
        if (!result.isPresent()) return;
        List<Adviser> lst = JpaManager.getInstance().getEm().createQuery("SELECT t FROM Adviser t WHERE t.number=:p", Adviser.class)
                .setParameter("p",  Integer.parseInt(result.get())).getResultList();
        if (lst.isEmpty()) {
            messageBox("An exception", "This referee does not exist",Alert.AlertType.ERROR);
            return;
        }
        selectedTour.getAdviserList().remove(lst.get(0));
        JpaManager.getInstance().getEm().merge(selectedTour);
        loadTours();
    }

    private Tournament checker(){
        if (fieldName.getText().length() == 0){
            messageBox("An exception", "You must fill the name field",Alert.AlertType.ERROR);
            return null;
        }
        if (fieldId.getText().length() == 0){
            messageBox("An exception", "You must fill the referee id field",Alert.AlertType.ERROR);
            return null;
        }
        if (!fieldId.getText().matches("\\d*")) {
            messageBox("An exception", "RefereeID only numbers are allowed",Alert.AlertType.ERROR);
            return null;
        }
        Tournament t = new Tournament();
        t.setName(fieldName.getText());
        t.setDate(Date.from(datePicker.getValue().atStartOfDay(ZoneId.of("Europe/Prague")).toInstant()));
        t.setClub_name(comboClub.getValue());
        t.setLeague(comboLeague.getValue());
        return t;
    }

    private void messageBox(String header, String message, Alert.AlertType type){
        Exception e = new Exception(header);
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));

        Alert alert = new Alert(type);
        alert.setHeaderText(message);
        alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea(sw.toString())));
        alert.showAndWait();
    }
    @FXML
    void initialize(){
        fieldId.textProperty().addListener((obs, oldValue, newValue) ->{
            int maxSize = 6;
            if (fieldId.getText().length() > maxSize){
                fieldId.setText(fieldId.getText().substring(0, maxSize));
            }
        });
        comboLeague.getItems().add("Gambrinus");
        comboLeague.getItems().add("Pilsner");
        comboLeague.getItems().add("Extra league");
        comboLeague.getItems().add("Potato league");
        comboLeague.getSelectionModel().selectFirst();
        datePicker.setValue(LocalDate.now());
        EntityManager em = JpaManager.getInstance().getEm();
        ClubDao cd = new ClubDao(em);
        em.getTransaction().begin();
        List<Club> clubs = cd.findAll();
        em.getTransaction().commit();
        for (Club c : clubs){
            comboClub.getItems().add(c.getName());
        }
        comboClub.getSelectionModel().selectFirst();
        for (TableColumn<Tournament, ?> column : tourTable.getColumns()){
            column.setCellValueFactory(new PropertyValueFactory<>(column.getId()));
        }
        loadTours();
        tourTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                selectedTour = obs.getValue();
                loadTournamentFromTable(selectedTour);
            }
        });
    }

    private void loadTournamentFromTable(Tournament t){
        fieldName.setText(t.getName());
        datePicker.setValue(t.getDate().toInstant().atZone(ZoneId.of("Europe/Prague")).toLocalDate());
        comboClub.setValue(t.getClub_name());
        comboLeague.setValue(t.getLeague());
    }

    private void loadTours(){
        EntityManager em = JpaManager.getInstance().getEm();
        TournamentDao td = new TournamentDao(em);
        em.getTransaction().begin();
        List<Tournament> tours = td.findAll();
        em.getTransaction().commit();
        tourTable.getItems().clear();
        for (Tournament t : tours){
            tourTable.getItems().add(t);
        }
        selectedTour = null;
    }
}
