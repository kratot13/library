package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller {
    @FXML
    Button btnTest;
    public void TestButtonClick(ActionEvent event) {
        System.out.println(btnTest.getId());
        ScreenController.getInstance().activate("addTour");
    }
}
