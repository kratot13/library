package controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.util.HashMap;

public class ScreenController {
    private final HashMap<String, Pane> screenMap = new HashMap<>();
    private final Scene main;

    private static ScreenController single_instance = null;

    private ScreenController() {
        Pane p = null;
        try{
            p = FXMLLoader.load(getClass().getResource("/main.fxml"));

        } catch (IOException e){
            System.out.println("Failed to load main.xml\n" + e.getMessage());
        }
        assert p != null;
        addScreen("main", p);
        main = new Scene(p, p.prefWidth(400), p.prefHeight(400));
    }

    public void addScreen(String name, Pane pane){
        screenMap.put(name, pane);
    }

    public void removeScreen(String name){
        screenMap.remove(name);
    }

    public Scene activate(String name) {
        if (!screenMap.containsKey(name)) {
            System.out.println(name + " is not added to map yet!");
            return null;
        }
        main.setRoot( screenMap.get(name) );
        return main;
    }

    public static ScreenController getInstance(){
        if (single_instance == null){
            single_instance = new ScreenController();
        }
        return single_instance;
    }
}
