package com.example.eventfinder

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class EventFinderApplication

fun main(args: Array<String>) {
	runApplication<EventFinderApplication>(*args)
}
