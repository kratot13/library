package com.example.eventfinder.projection

import com.example.eventfinder.model.EventType
import java.time.LocalDateTime

/**
 * A Projection for the [com.example.eventfinder.model.Event] entity
 */
interface EventListView {
    val dateTime: LocalDateTime
    val duration: Int
    val eventType: EventType
}