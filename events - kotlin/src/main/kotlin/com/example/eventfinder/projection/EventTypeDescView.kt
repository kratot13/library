package com.example.eventfinder.projection

/**
 * A Projection for the [com.example.eventfinder.model.Sport] entity
 */
interface EventTypeDescView {
    val id: Long?
    val description: String?
}