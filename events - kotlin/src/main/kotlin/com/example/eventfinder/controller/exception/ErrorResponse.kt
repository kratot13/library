package com.example.eventfinder.controller.exception

data class ErrorResponse(val code: Int, val message: String)