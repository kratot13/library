package com.example.eventfinder.controller

import com.example.eventfinder.model.Event
import com.example.eventfinder.projection.EventListView
import com.example.eventfinder.service.EventService
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/events")
class EventController(override val service: EventService) : AbstractController<Event>(service) {

    // TODO get userId from session context since user creating the event is also its creator
    @PostMapping("/creator/{userId}/EventType/{EventTypeId}")
    fun save(@PathVariable userId: Long, @PathVariable eventTypeId: Long, @RequestBody event: Event) {
        service.createEvent(userId, eventTypeId, event)
    }

    @GetMapping("/list")
    fun getEventList(): Iterable<EventListView> {
        return service.getEventList()
    }
}