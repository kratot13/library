package com.example.eventfinder.controller.exception

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class ExceptionHandlerController {
    @ExceptionHandler(EmptyResultDataAccessException::class)
    fun handleEmptyResultDataAccessException(
            ex: EmptyResultDataAccessException,
            request: WebRequest
    ): ResponseEntity<Any> {
        val message = "Entity not found"
        val errorResponse = ErrorResponse(HttpStatus.NOT_FOUND.value(), message)
        return ResponseEntity(errorResponse, HttpHeaders(), HttpStatus.NOT_FOUND)
    }
}