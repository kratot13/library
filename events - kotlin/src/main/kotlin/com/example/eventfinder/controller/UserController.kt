package com.example.eventfinder.controller

import com.example.eventfinder.model.User
import com.example.eventfinder.service.UserService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/users")
class UserController(override val service: UserService) : AbstractController<User>(service)