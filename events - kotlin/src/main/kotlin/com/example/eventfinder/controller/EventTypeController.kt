package com.example.eventfinder.controller

import com.example.eventfinder.model.EventType
import com.example.eventfinder.projection.EventTypeDescView
import com.example.eventfinder.service.EventTypeService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/EventTypes")
class EventTypeController(override val service: EventTypeService) : AbstractController<EventType>(service) {
    @GetMapping("/descs")
    fun getDescs(): Iterable<EventTypeDescView> {
        return service.findAllEventTypesDescs()
    }

    @GetMapping("/descs/{id}")
    fun getDescs(@PathVariable id: Long): ResponseEntity<EventTypeDescView> {
        return service.findEventTypesDescsById(id)?.let { ResponseEntity.ok().body(it) } ?: ResponseEntity.notFound()
            .build()
    }
}