package com.example.eventfinder.controller

import com.example.eventfinder.model.AbstractEntity
import com.example.eventfinder.service.AbstractService
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

abstract class AbstractController<T: AbstractEntity>(
    protected open val service: AbstractService<T>
) {
    @GetMapping("", "/")
    fun findAll(): Iterable<T> {
        return service.findAll()
    }

    @GetMapping("/count")
    fun getCount(): Long {
        return service.count()
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<T> {
        return service.findById(id)?.let{ ResponseEntity.ok().body(it) } ?: ResponseEntity.notFound().build()
    }

    @PostMapping("", "/")
    fun save(@Valid @RequestBody entity: T){
        service.save(entity)
    }

    @PutMapping("", "/")
    fun update(@Valid @RequestBody entity: T){
        service.update(entity)
    }

    @DeleteMapping("", "/")
    fun deleteAll() {
        service.deleteAll()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long) {
        service.delete(id)
    }

}