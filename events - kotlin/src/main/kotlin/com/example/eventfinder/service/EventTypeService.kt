package com.example.eventfinder.service

import com.example.eventfinder.model.EventType
import com.example.eventfinder.projection.EventTypeDescView
import com.example.eventfinder.repository.EventTypeRepository
import jakarta.transaction.Transactional
import org.springframework.stereotype.Service

@Service
@Transactional
class EventTypeService(override val repository: EventTypeRepository) : AbstractService<EventType>(repository) {
    fun findAllEventTypesDescs(): Iterable<EventTypeDescView> {
        return repository.findAllBy(EventTypeDescView::class.java)
    }

    fun findEventTypesDescsById(id: Long): EventTypeDescView? {
        return repository.findById(id, EventTypeDescView::class.java)
    }
}