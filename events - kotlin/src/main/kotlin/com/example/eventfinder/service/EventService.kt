package com.example.eventfinder.service

import com.example.eventfinder.exception.NotFoundException
import com.example.eventfinder.model.Event
import com.example.eventfinder.model.EventType
import com.example.eventfinder.model.User
import com.example.eventfinder.projection.EventListView
import com.example.eventfinder.repository.EventRepository
import com.example.eventfinder.repository.EventTypeRepository
import com.example.eventfinder.repository.UserRepository
import jakarta.transaction.Transactional
import org.springframework.stereotype.Service


@Service
@Transactional
class EventService(
    override val repository: EventRepository,
    val userRepository: UserRepository,
    val eventTypeRepository: EventTypeRepository,
) : AbstractService<Event>(repository) {

    fun createEvent(userId: Long, EventTypeId: Long, eventDto: Event) {
        val user: User = userRepository.findById(userId).orElseThrow { NotFoundException("USER NOT FOUND") }
        val eventType: EventType = eventTypeRepository.findById(EventTypeId).orElseThrow { NotFoundException("EventType NOT FOUND") }
        eventDto.creator = user
        eventDto.eventType = eventType
        repository.save(eventDto)
    }

    fun getEventList(): Iterable<EventListView> {
        return repository.findAllBy(EventListView::class.java)
    }
}