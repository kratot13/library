package com.example.eventfinder.service

import com.example.eventfinder.model.User
import com.example.eventfinder.repository.UserRepository
import jakarta.transaction.Transactional
import org.springframework.stereotype.Service


@Service
@Transactional
class UserService(override val repository: UserRepository) : AbstractService<User>(repository)