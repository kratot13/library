package com.example.eventfinder.service

import com.example.eventfinder.model.AbstractEntity
import com.example.eventfinder.repository.AbstractRepository
import org.springframework.data.repository.findByIdOrNull

abstract class AbstractService<T: AbstractEntity>(
    protected open val repository: AbstractRepository<T, Long>
) {

    fun findAll(): Iterable<T> {
        return repository.findAll()
    }

    fun findById(id: Long): T? {
        return repository.findByIdOrNull(id)
    }

    fun save(entity: T) {
        repository.save(entity)
    }

    fun update(entity: T) {
        repository.save(entity)
    }

    fun delete(id: Long) {
        repository.deleteById(id)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

    fun delete(entity: T) {
        repository.delete(entity)
    }

    fun count(): Long {
        return repository.count()
    }

}