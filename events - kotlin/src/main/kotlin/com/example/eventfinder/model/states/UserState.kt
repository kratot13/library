package com.example.eventfinder.model.states

enum class UserState {
    REGISTERED,
    IN_PROGRESS,
    VERIFIED,
    WARNED,
    BANNED
}