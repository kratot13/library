package com.example.eventfinder.model

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.CascadeType
import jakarta.persistence.Entity
import jakarta.persistence.Lob
import jakarta.persistence.OneToMany

@Entity
data class EventType(
    val EventType: String,
    @Lob
    val description: String,
    @OneToMany(mappedBy = "eventType", cascade = [CascadeType.REMOVE], orphanRemoval = true)
    @JsonIgnore
    val events: MutableList<Event> = mutableListOf()
) : AbstractEntity()