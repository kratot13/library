package com.example.eventfinder.model.states

enum class EventState {
    ACTIVE,
    TOBE,
    CANCELED,
    MOVED,
}