package com.example.eventfinder.model

import jakarta.persistence.*

@Entity
data class Rating (
    @Lob
    var comment: String? = null,
    val score: Int,
    @ManyToOne(cascade = [CascadeType.PERSIST], optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    val user: User
) : AbstractEntity()