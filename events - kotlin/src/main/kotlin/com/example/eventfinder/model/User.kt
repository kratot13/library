package com.example.eventfinder.model

import com.example.eventfinder.model.states.UserState
import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import jakarta.validation.constraints.Email

@Entity
@Table(name = "user_account")
data class User(
    @Column(nullable = false)
    val nickname: String,
    val age: Int?,
    @Column(nullable = false, unique = true)
    @get:Email(message = "Invalid email address")
    val email: String,
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    val gender: Gender,
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    val userState: UserState,
    val phoneNumber: String?,
    @Lob
    val lifestory: String?,
    val profilePic: String?,
    @OneToMany(mappedBy = "user", cascade = [CascadeType.REMOVE], orphanRemoval = true)
    @JsonIgnore
    val ratings: MutableList<Rating> = mutableListOf(),
    @OneToMany(mappedBy = "creator", cascade = [CascadeType.REMOVE], orphanRemoval = true)
    @JsonIgnore
    val createdEvents: MutableList<Event> = mutableListOf()
) : AbstractEntity()