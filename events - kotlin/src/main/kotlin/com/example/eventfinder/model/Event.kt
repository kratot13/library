package com.example.eventfinder.model

import com.example.eventfinder.model.states.EventState
import jakarta.persistence.*
import jakarta.validation.constraints.Future
import java.time.LocalDateTime

@Entity
data class Event (
    @Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Future
    val dateTime: LocalDateTime,
    val duration: Int,
    val priceEst: Int,
    @Lob
    var description: String? = null,
    @Enumerated
    val eventState: EventState,
    @ManyToOne(cascade = [CascadeType.PERSIST], optional = false)
    var creator: User,
    @ManyToOne(cascade = [CascadeType.PERSIST], optional = false)
    var eventType: EventType
) : AbstractEntity()