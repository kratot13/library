package com.example.eventfinder.model

enum class Gender {
    MALE,
    FEMALE,
    OTHER,
}