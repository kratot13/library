package com.example.eventfinder.model

import jakarta.persistence.*
import java.time.LocalDateTime

@MappedSuperclass
abstract class AbstractEntity (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    @Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    var creationDate: LocalDateTime? = null
) {
    @PrePersist
    open fun prePersist() {
        creationDate = LocalDateTime.now()
    }
}