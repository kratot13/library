package com.example.eventfinder.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class DemoConfiguration {

    @Value("\${demo.var}")
    lateinit var demoVar: String

    @Bean
    fun demoBean(): String = demoVar
}