package com.example.eventfinder.configuration

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import java.io.FileInputStream


@Configuration
class FirebaseConfiguration(
    @Value("\${firebase.credential.resource-path}")
    private val keyPath: String
) {
    @Bean
    fun firebaseApp() {
        val resource: Resource = ClassPathResource(keyPath)
        val serviceAccount = FileInputStream(resource.file)
        val options = FirebaseOptions.builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .build()

        if (FirebaseApp.getApps().isEmpty()) {
            FirebaseApp.initializeApp(options)
        }
    }
}