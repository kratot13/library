package com.example.eventfinder.exception

data class NotFoundException(
    private val errorCode: String,
    private val errorDesc: String?
) : RuntimeException() {
    constructor(errorCode: String) : this(errorCode, null)
}