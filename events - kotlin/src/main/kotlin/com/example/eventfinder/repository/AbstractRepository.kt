package com.example.eventfinder.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface AbstractRepository<T, ID> : CrudRepository<T, ID> {
    /**
     * Retrieves all entities from the database and maps them to the specified projection class.
     *
     * @param projection The projection class to map the entities to.
     * @return An iterable of entities mapped to the specified projection class.
     */
    fun <S> findAllBy(projection: Class<S>): Iterable<S>

    /**
     * Retrieves the entity of type [S] with the given [id] projected to the specified [projection] class, if it exists.
     *
     * @param id The identifier of the entity to retrieve.
     * @param projection The class of the projection to use when retrieving the entity.
     * @return The entity projected to the specified [projection] class, or null if no such entity exists.
     */
    fun <S> findById(id: ID, projection: Class<S>): S?
}