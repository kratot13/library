package com.example.eventfinder.repository

import com.example.eventfinder.model.Event

interface EventRepository : AbstractRepository<Event, Long>