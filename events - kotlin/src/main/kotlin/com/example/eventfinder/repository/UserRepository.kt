package com.example.eventfinder.repository

import com.example.eventfinder.model.User

interface UserRepository : AbstractRepository<User, Long>