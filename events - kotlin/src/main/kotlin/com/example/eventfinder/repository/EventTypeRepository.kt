package com.example.eventfinder.repository

import com.example.eventfinder.model.EventType

interface EventTypeRepository : AbstractRepository<EventType, Long>