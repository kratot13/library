package com.example.eventfinder.security

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseToken
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.util.WebUtils


@Component
class TokenFilter(
    private val securityUtils: SecurityUtils
) : OncePerRequestFilter() {
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val cookie = WebUtils.getCookie(request, "JSESSIONID")
        if (cookie == null) filterFirebase(request)
        filterChain.doFilter(request, response)
    }

    private fun filterFirebase(request: HttpServletRequest) {
        val idToken = securityUtils.getTokenFromRequest(request)
        var decodedToken: FirebaseToken? = null
        try {
            decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken)
        } catch (e: FirebaseAuthException) {
            logger.error("Firebase Exception ${e.message}")
        } catch (e: Exception) {
            logger.error("Exception ${e.message}")
        }
        if (decodedToken != null) {
            val customPrincipal =
                CustomPrincipal(decodedToken.uid, decodedToken.email, decodedToken.isEmailVerified)
            val authentication = UsernamePasswordAuthenticationToken(
                customPrincipal, decodedToken, null
            )
            authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
            SecurityContextHolder.getContext().authentication = authentication
        }
    }
}