package com.example.eventfinder.security

data class CustomPrincipal(
    private val uid: String,
    private val email: String,
    private val emailVerified: Boolean?
)