package com.example.eventfinder.security

import jakarta.servlet.http.HttpServletRequest
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils


@Component
class SecurityUtils {
    fun getTokenFromRequest(request: HttpServletRequest): String? {
        var token: String? = null
        val bearerToken = request.getHeader("Authorization")
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            token = bearerToken.substring(7, bearerToken.length)
        }
        return token
    }
}