package cz.cvut.fel.sin.service;

import cz.cvut.fel.sin.dao.AuthorDao;
import cz.cvut.fel.sin.dao.PublisherDao;
import cz.cvut.fel.sin.model.Author;
import cz.cvut.fel.sin.model.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class PublisherService {

    private final PublisherDao dao;

    @Autowired
    public PublisherService(PublisherDao dao) {
        this.dao = dao;
    }

    @Transactional
    public void addPublisher(Publisher pub) {
        Objects.requireNonNull(pub);
        dao.persist(pub);
    }
    @Transactional(readOnly = true)
    public List<Publisher> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public Publisher findByName(String name) {
        return dao.find(name);
    }
}
