package cz.cvut.fel.sin.controller;

import cz.cvut.fel.sin.model.Author;
import cz.cvut.fel.sin.model.Contract;
import cz.cvut.fel.sin.service.AuthorService;
import cz.cvut.fel.sin.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ContractController {
    private final ContractService contractService;
    private final AuthorService authorService;
    @Autowired
    public ContractController(ContractService contractService, AuthorService authorService) {
        this.contractService = contractService;
        this.authorService = authorService;
    }
    @GetMapping("/contracts")
    public List<Contract> getContracts() {
        return contractService.findAll();
    }

    @GetMapping("/contracts/{email}")
    public List<Contract> getContractsByAuthor(@PathVariable String email){
        Author a = authorService.findByEmail(email);
        if (a == null){
            return new ArrayList<>();
        }
        return contractService.findByEmail(email);
    }

    @PutMapping(value = "/contract",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Contract addContract(@RequestBody Contract contract){
        contractService.addContract(contract);
        return contract;
    }
}
