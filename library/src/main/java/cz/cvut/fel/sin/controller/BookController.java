package cz.cvut.fel.sin.controller;

import cz.cvut.fel.sin.model.Book;
import cz.cvut.fel.sin.model.Publisher;
import cz.cvut.fel.sin.service.BookService;
import cz.cvut.fel.sin.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookController {
    private final BookService bookService;
    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }
    @GetMapping("/books")
    public List<Book> getBooks() {
        return bookService.findAll();
    }

    @PutMapping(value = "/book",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Book addBook(@RequestBody Book book){
        bookService.addBook(book);
        return book;
    }
}
