package cz.cvut.fel.sin.dao;


import cz.cvut.fel.sin.model.Member;
import cz.cvut.fel.sin.model.Publisher;
import org.springframework.stereotype.Repository;

@Repository
public class PublisherDao extends BaseDao<Publisher>{

    public PublisherDao() {
        super(Publisher.class);
    }
}
