package cz.cvut.fel.sin.dao;


import cz.cvut.fel.sin.model.Author;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorDao extends BaseDao<Author>{

    public AuthorDao() {
        super(Author.class);
    }
}
