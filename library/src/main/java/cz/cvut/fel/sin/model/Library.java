package cz.cvut.fel.sin.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "LIBRARY")
public class Library {

    @Id
    @Column(name = "NAME")
    private String name;
    @Column(name = "ADDRESS")
    private String address;

    @ManyToMany(mappedBy = "libraries")
    Set<Member> members;
}
