package cz.cvut.fel.sin.dao;


import cz.cvut.fel.sin.model.Book;
import org.springframework.stereotype.Repository;

@Repository
public class BookDao extends BaseDao<Book>{

    public BookDao() {
        super(Book.class);
    }

    public boolean exists(Integer isbn) {
        return ! em.createQuery("SELECT e FROM " + type.getSimpleName() + " e where e.isbn = '" + isbn + "'", type).getResultList().isEmpty();
    }

    public int countOfBooksInLibrary(Integer isbn, String name){
        return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e where e.isbn = '" + isbn + "' and e.library = '" + name + "'", type).getResultList().size();
    }
}
