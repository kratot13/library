package cz.cvut.fel.sin.service;

import cz.cvut.fel.sin.dao.AuthorDao;
import cz.cvut.fel.sin.dao.BookDao;
import cz.cvut.fel.sin.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class AuthorService {

    private final AuthorDao dao;

    @Autowired
    public AuthorService(AuthorDao dao) {
        this.dao = dao;
    }

    @Transactional
    public void addAuthor(Author author) {
        Objects.requireNonNull(author);
        dao.persist(author);
    }
    @Transactional(readOnly = true)
    public List<Author> findAll() {
        return dao.findAll();
    }
    @Transactional(readOnly = true)
    public Author findByEmail(String email) {
        return dao.find(email);
    }
}
