package cz.cvut.fel.sin.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "AUTHOR")
public class Author {

    @Id
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "FNAME")
    private String fname;
    @Column(name = "SURNAME")
    private String surname;

    @JsonIgnore
    @ManyToMany(mappedBy = "authors", cascade = CascadeType.MERGE)
    private List<Book> books;

//    @OneToMany(mappedBy = "publisher")
//    private Set<Contract> contracts;

}
