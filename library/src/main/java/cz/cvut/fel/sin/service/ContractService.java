package cz.cvut.fel.sin.service;

import cz.cvut.fel.sin.dao.AuthorDao;
import cz.cvut.fel.sin.dao.ContractDao;
import cz.cvut.fel.sin.dao.PublisherDao;
import cz.cvut.fel.sin.model.Author;
import cz.cvut.fel.sin.model.Contract;
import cz.cvut.fel.sin.model.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class ContractService {

    private final ContractDao dao;
    private final AuthorDao authorDao;
    private final PublisherDao publisherDao;

    @Autowired
    public ContractService(ContractDao dao, AuthorDao authorDao, PublisherDao publisherDao) {
        this.dao = dao;
        this.authorDao = authorDao;
        this.publisherDao = publisherDao;
    }

    @Transactional
    public void addContract(Contract contract) {
        Objects.requireNonNull(contract);
        Author a = authorDao.find(contract.getAuthor().getEmail());
        contract.setAuthor(a);
        Publisher pub = publisherDao.find(contract.getPublisher().getName());
        contract.setPublisher(pub);
        dao.update(contract);
    }
    @Transactional(readOnly = true)
    public List<Contract> findAll() {
        return dao.findAll();
    }
    @Transactional(readOnly = true)
    public List<Contract> findByEmail(String email) {
        return dao.findByEmail(email);
    }
}
