package cz.cvut.fel.sin.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "MEMBER")
public class Member {

    @Id
    @GeneratedValue
    @Column(name = "PERSON_NUMBER")
    private Integer personNumber;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "FNAME")
    private String fname;
    @Column(name = "SURNAME")
    private String surname;
    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private MemberState state;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Library> libraries;

}
