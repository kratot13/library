package cz.cvut.fel.sin.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "BOOK")
public class Book {

    @Id
    @GeneratedValue
    private int id;
    @Column(name = "ISBN")
    private Integer isbn;
    @Column(name = "PUB_DATE")
    private String pub_date;
    @Column(name = "GENRE")
    private String genre;
    @Column(name = "TITLE")
    private String title;


    @ManyToMany(cascade = CascadeType.MERGE)
    List<Author> authors;

    @ManyToOne
    @JoinColumn(name="PUBLISHER", nullable=false)
    private Publisher publisher;

    @ManyToOne
    @JoinColumn(name="MEMBER")
    private Member member;

    @ManyToOne
    @JoinColumn(name="LIBRARY", nullable=false)
    private Library library;

}
