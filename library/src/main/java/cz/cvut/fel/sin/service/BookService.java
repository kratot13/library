package cz.cvut.fel.sin.service;

import cz.cvut.fel.sin.dao.AuthorDao;
import cz.cvut.fel.sin.dao.BookDao;
import cz.cvut.fel.sin.dao.ContractDao;
import cz.cvut.fel.sin.dao.LibraryDao;
import cz.cvut.fel.sin.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class BookService {

    private final BookDao dao;
    private final ContractDao contractDao;
    private final AuthorDao authorDao;
    private final LibraryDao libraryDao;

    @Autowired
    public BookService(BookDao dao, ContractDao contractDao, AuthorDao authorDao, LibraryDao libraryDao) {
        this.dao = dao;
        this.contractDao = contractDao;
        this.authorDao = authorDao;
        this.libraryDao = libraryDao;
    }

    @Transactional
    public Book addBook(Book book) {
        Objects.requireNonNull(book);
//        if (dao.exists(book.getIsbn())){
//            return null;
//        }
        List<Contract> pubContracts = contractDao.findByPubName(book.getPublisher().getName());
        for (int i = 0; i < book.getAuthors().size(); i++){
            Author a = authorDao.find(book.getAuthors().get(i).getEmail());
            book.getAuthors().set(i, a);
            boolean found = false;
            for (Contract c : pubContracts){
                if (c.getAuthor().getEmail().equals(a.getEmail())){
                    found = true;
                }
            }
            if (!found)return null;
        }
        if (dao.countOfBooksInLibrary(book.getIsbn(), book.getLibrary().getName()) >= 5){
            return null;
        }
        dao.update(book);
        return book;
    }
    @Transactional(readOnly = true)
    public List<Book> findAll() {
        return dao.findAll();
    }
}
