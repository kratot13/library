package cz.cvut.fel.sin.model;

public interface AbstractMember {
    String getDescription();
    int ordinal();
}
