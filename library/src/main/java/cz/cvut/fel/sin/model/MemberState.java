package cz.cvut.fel.sin.model;

import lombok.Getter;

@Getter
public enum MemberState implements AbstractMember {

    ACTIVE("Active"),
    GOLDEN("Golden"),
    TRIAL("Trial"),
    CANCELLED("Cancelled");

    private final String description;

    MemberState(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return null;
    }
}
