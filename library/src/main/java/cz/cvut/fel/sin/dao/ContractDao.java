package cz.cvut.fel.sin.dao;


import cz.cvut.fel.sin.model.Book;
import cz.cvut.fel.sin.model.Contract;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import java.util.List;

@Repository
public class ContractDao extends BaseDao<Contract>{

    public ContractDao() {
        super(Contract.class);
    }

    public List<Contract> findByEmail(String email) {
        try {
            return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e where e.author = '" + email + "'", type).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }

    public List<Contract> findByPubName(String name) {
        try {
            return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e where e.publisher = '" + name + "'", type).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
