package cz.cvut.fel.sin.controller;

import cz.cvut.fel.sin.model.Author;
import cz.cvut.fel.sin.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuthorController {
    private final AuthorService authorService;
    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }
    @GetMapping("/authors")
    public List<Author> getAuthors() {
        return authorService.findAll();
    }

    @PutMapping(value = "/author",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Author addAuthor(@RequestBody Author author){
        authorService.addAuthor(author);
        return author;
    }
}
