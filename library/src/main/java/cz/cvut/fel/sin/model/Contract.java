package cz.cvut.fel.sin.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "CONTRACT")
public class Contract {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "NUMBER")
    private String number;
    @Column(name = "START_DATE")
    private String startDate;
    @Column(name = "END_DATE")
    private String endDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="AUTHOR", nullable=false)
    private Author author;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="PUBLISHER", nullable=false)
    private Publisher publisher;

}
