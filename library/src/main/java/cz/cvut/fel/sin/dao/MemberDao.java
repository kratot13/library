package cz.cvut.fel.sin.dao;


import cz.cvut.fel.sin.model.Library;
import cz.cvut.fel.sin.model.Member;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDao extends BaseDao<Member>{

    public MemberDao() {
        super(Member.class);
    }
}
