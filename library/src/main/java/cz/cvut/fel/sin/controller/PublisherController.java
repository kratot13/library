package cz.cvut.fel.sin.controller;

import cz.cvut.fel.sin.model.Author;
import cz.cvut.fel.sin.model.Publisher;
import cz.cvut.fel.sin.service.AuthorService;
import cz.cvut.fel.sin.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PublisherController {
    private final PublisherService publisherService;
    @Autowired
    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }
    @GetMapping("/publishers")
    public List<Publisher> getAuthors() {
        return publisherService.findAll();
    }

    @PutMapping(value = "/publisher",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Publisher addPublisher(@RequestBody Publisher publisher){
        publisherService.addPublisher(publisher);
        return publisher;
    }
}
