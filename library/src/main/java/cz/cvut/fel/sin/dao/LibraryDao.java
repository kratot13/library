package cz.cvut.fel.sin.dao;


import cz.cvut.fel.sin.model.Contract;
import cz.cvut.fel.sin.model.Library;
import org.springframework.stereotype.Repository;

@Repository
public class LibraryDao extends BaseDao<Library>{

    public LibraryDao() {
        super(Library.class);
    }
}
