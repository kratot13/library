package cz.cvut.fel.sin.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "PUBLISHER")
public class Publisher {

    @Id
    @Column(name = "NAME")
    private String name;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "OWNER")
    private String owner;

//    @OneToMany(mappedBy = "publisher")
//    private Set<Contract> contracts;

}
