# Projects

## Tournament
Projekt byl na databázový předmět, šlo hlavně o práci s JPA.
## Pictionary
Projekt vytvořený s kolegou (zasílám pouze svou backendovou část) na předmět zaměřený pouze na Javu.
## Library
Library je z předmětu softwarové inženýrství, lehký náznak springu.
## Events
Toto je můj osobní projekt, kde jsem přejmenoval většinu tříd od originální verze (proto nemusí dávat názvy smysl). Je tam implementovaná i autentikace přes Firebase.


